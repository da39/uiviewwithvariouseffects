//
//  ViewController.swift
//  UIViewWithVariousEffects
//
//  Created by Ruslan Mavlyutov on 07/03/2019.
//  Copyright © 2019 Ruslan Mavlyutov. All rights reserved.
//

import UIKit
import UIViewWithVariousEffects

final class ViewController: UIViewController {

    struct StringsProperties {
        static let show = "Show subviews"
        static let close = "Hide subviews"
    }

    @IBOutlet weak var placeholderStateView: ViewWithRoundedCorners!

    @IBOutlet weak var shadowView: ShadowView!

    @IBOutlet weak var showViewButton: UIButton!

    @IBOutlet weak var shadowSwitcher: UISwitch!
    @IBOutlet weak var circleShadowLabel: UILabel!
    @IBOutlet weak var rectangleShadowLabel: UILabel!

    @IBOutlet weak var topRightCornerLabel: UILabel!
    @IBOutlet weak var topLeftCornerLabel: UILabel!
    @IBOutlet weak var bottomLeftCornerLabel: UILabel!
    @IBOutlet weak var bottomRightCornerLabel: UILabel!

    @IBOutlet weak var topLeftSwitcher: UISwitch!
    @IBOutlet weak var topRightSwithcer: UISwitch!
    @IBOutlet weak var bottomLeftSwitcher: UISwitch!
    @IBOutlet weak var bottomRightSwitcher: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

        showViewButton.setTitle(StringsProperties.show, for: .normal)
        changeState(true)
        startInit()
    }

    func startInit() {
        shadowView.configureShadow(shadowType: .rectangle)
        placeholderStateView.roundTopLeftCorner = true
        placeholderStateView.roundBottomRightCorner = true
    }

    @IBAction func showView(_ sender: Any) {
        if showViewButton.titleLabel?.text == StringsProperties.show {
            changeState(false)
            showViewButton.setTitle(StringsProperties.close, for: .normal)
        } else if showViewButton.titleLabel?.text == StringsProperties.close {
            changeState(true)
            showViewButton.setTitle(StringsProperties.show, for: .normal)
        }
    }

    @IBAction func shadowSwitch(_ sender: UISwitch) {
        if sender.isOn {
            shadowView.configureShadow(shadowType: .rectangle)
        } else {
            shadowView.configureShadow(shadowType: .circle)
        }
    }

    @IBAction func roundTopLeftCornerSwitch(_ sender: UISwitch) {
        if sender.isOn {
            placeholderStateView.roundTopLeftCorner = true
        } else {
            placeholderStateView.roundTopLeftCorner = false
        }
    }

    @IBAction func roundTopRightCornerSwitcher(_ sender: UISwitch) {
        if sender.isOn {
            placeholderStateView.roundTopRightCorner = true
        } else {
            placeholderStateView.roundTopRightCorner = false
        }
    }

    @IBAction func roundBottomLeftCornerSwitcher(_ sender: UISwitch) {
        if sender.isOn {
            placeholderStateView.roundBottomLeftCorner = true
        } else {
            placeholderStateView.roundBottomLeftCorner = false
        }
    }

    @IBAction func roundBottomRightCornerSwitcher(_ sender: UISwitch) {
        if sender.isOn {
            placeholderStateView.roundBottomRightCorner = true
        } else {
            placeholderStateView.roundBottomRightCorner = false
        }
    }

    private func changeState(_ isHided: Bool) {
        placeholderStateView.isHidden = isHided
        shadowView.isHidden = isHided

        shadowSwitcher.isHidden = isHided
        circleShadowLabel.isHidden = isHided
        rectangleShadowLabel.isHidden = isHided

        topLeftCornerLabel.isHidden = isHided
        topRightCornerLabel.isHidden = isHided
        bottomLeftCornerLabel.isHidden = isHided
        bottomRightCornerLabel.isHidden = isHided

        topLeftSwitcher.isHidden = isHided
        topRightSwithcer.isHidden = isHided
        bottomLeftSwitcher.isHidden = isHided
        bottomRightSwitcher.isHidden = isHided
    }
}
