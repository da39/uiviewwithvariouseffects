//
//  ShadowType.swift
//  Lets Roam
//
//  Created by Alex Martynov on 1/3/19.
//  Copyright © 2019 Let's Roam. All rights reserved.
//

import Foundation

public enum ShadowType {
    case rectangle
    case circle
}
