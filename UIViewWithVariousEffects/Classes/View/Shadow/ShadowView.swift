//
//  ShadowView.swift
//  Lets Roam
//
//  Created by Alex Martynov on 1/3/19.
//  Copyright © 2019 Mercdev. All rights reserved.
//

import UIKit

@objc @IBDesignable final public class ShadowView: UIView {
    private var shadowType: ShadowType

    override init(frame: CGRect) {
        self.shadowType = .circle

        super.init(frame: frame)

        self.clipsToBounds = false
    }

    required init?(coder aDecoder: NSCoder) {
        self.shadowType = .circle

        super.init(coder: aDecoder)

        self.clipsToBounds = false
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        updateShadow()
    }

    public func configureShadow(shadowType: ShadowType) {
        self.shadowType = shadowType
        updateShadow()
    }
}

// MARK: - private

private extension ShadowView {
    func updateShadow() {
        layer.shadowPath = createShadowPath()
        layer.shadowRadius = 4
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowOpacity = 0.5
    }

    func createShadowPath() -> CGPath {
        switch shadowType {
        case .rectangle:
            return UIBezierPath(rect: bounds).cgPath
        case .circle:
            return UIBezierPath(roundedRect: bounds, cornerRadius: bounds.height / 2).cgPath
        }
    }
}
