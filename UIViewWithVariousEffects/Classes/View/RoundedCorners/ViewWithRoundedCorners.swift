//
//  ViewWithRoundedCorners.swift
//  eventicious
//
//  Created by Minibayev Ramil on 23/11/2018.
//  Copyright © 2018 Mercdev. All rights reserved.
//

import UIKit.UIView

@IBDesignable final public class ViewWithRoundedCorners: UIView {
    private var roundingRadius: CGFloat = 0.0 {
        didSet { updateMask() }
    }

    override public var cornerRadius: CGFloat {
        get { return roundingRadius }
        set { roundingRadius = max(0, newValue) }
    }

    @IBInspectable public var roundTopLeftCorner: Bool = false {
        didSet { updateMask() }
    }

    @IBInspectable public var roundTopRightCorner: Bool = false {
        didSet { updateMask() }
    }

    @IBInspectable public var roundBottomLeftCorner: Bool = false {
        didSet { updateMask() }
    }

    @IBInspectable public var roundBottomRightCorner: Bool = false {
        didSet { updateMask() }
    }

    private var lastSize: CGSize = .zero

    override public func layoutSubviews() {
        super.layoutSubviews()

        if #available(iOS 11, *) {
            return
        } else if !lastSize.isEqualOnScreen(to: bounds.size) {
            lastSize = bounds.size
            updateLayerMask()
        }
    }
}

private extension ViewWithRoundedCorners {
    var allCornersRounded: Bool {
        return roundTopLeftCorner && roundTopRightCorner && roundBottomLeftCorner && roundBottomRightCorner
    }

    var noCornersRounded: Bool {
        return !(roundTopLeftCorner || roundTopRightCorner || roundBottomLeftCorner || roundBottomRightCorner)
    }

    func updateMask() {
        if #available(iOS 11, *) {
            updateCornerMask()
        } else {
            updateLayerMask()
        }
    }
    
    @available(iOS, deprecated: 11)
    func updateLayerMask() {
        clipsToBounds = true

        guard !allCornersRounded, !noCornersRounded, cornerRadius > 0  else {
            layer.cornerRadius = allCornersRounded ? cornerRadius : 0
            layer.mask = nil
            return
        }

        var cornersToRound: UIRectCorner = []

        if roundTopLeftCorner {
            cornersToRound.insert(.topLeft)
        }
        if roundTopRightCorner {
            cornersToRound.insert(.topRight)
        }
        if roundBottomLeftCorner {
            cornersToRound.insert(.bottomLeft)
        }
        if roundBottomRightCorner {
            cornersToRound.insert(.bottomRight)
        }
        
        let cornerSize = CGSize(width: cornerRadius, height: cornerRadius)
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: cornersToRound,
                                    cornerRadii: cornerSize)
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }

    @available(iOS 11, *)
    func updateCornerMask() {
        clipsToBounds = true

        guard !noCornersRounded, cornerRadius > 0 else {
            layer.cornerRadius = 0
            layer.maskedCorners = []
            return
        }

        layer.cornerRadius = cornerRadius
        var maskedCorners: CACornerMask = []

        if roundTopLeftCorner {
            maskedCorners.insert(.layerMinXMinYCorner)
        }
        if roundTopRightCorner {
            maskedCorners.insert(.layerMaxXMinYCorner)
        }
        if roundBottomLeftCorner {
            maskedCorners.insert(.layerMinXMaxYCorner)
        }
        if roundBottomRightCorner {
            maskedCorners.insert(.layerMaxXMaxYCorner)
        }

        layer.maskedCorners = maskedCorners
    }
}
