//
//  UIView+LayerInterfaceBuilderProperties.swift
//  eventicious
//
//  Created by denisov on 18/10/2018.
//  Copyright © 2018 Mercdev. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    @IBInspectable var borderColor: UIColor? {
        get {
            guard let cgColor = self.layer.borderColor else {
                return nil
            }
            return UIColor.init(cgColor: cgColor)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
}
