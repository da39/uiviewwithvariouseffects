//
//  CGSize+ScreenComparison.swift
//  eventicious
//
//  Created by Minibayev Ramil on 26/11/2018.
//  Copyright © 2018 Mercdev. All rights reserved.
//
import UIKit

extension CGSize {
    func isEqualOnScreen(to otherSize: CGSize) -> Bool {
        let hasSameWidth = abs(width - otherSize.width) < 1 / UIScreen.main.scale
        let hasSameHeight = abs(height - otherSize.height) < 1 / UIScreen.main.scale
        return hasSameWidth && hasSameHeight
    }
}
