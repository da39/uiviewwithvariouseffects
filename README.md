# UIViewWithVariousEffects

[![CI Status](https://img.shields.io/travis/RuslanMavlyutov/UIViewWithVariousEffects.svg?style=flat)](https://travis-ci.org/RuslanMavlyutov/UIViewWithVariousEffects)
[![Version](https://img.shields.io/cocoapods/v/UIViewWithVariousEffects.svg?style=flat)](https://cocoapods.org/pods/UIViewWithVariousEffects)
[![License](https://img.shields.io/cocoapods/l/UIViewWithVariousEffects.svg?style=flat)](https://cocoapods.org/pods/UIViewWithVariousEffects)
[![Platform](https://img.shields.io/cocoapods/p/UIViewWithVariousEffects.svg?style=flat)](https://cocoapods.org/pods/UIViewWithVariousEffects)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UIViewWithVariousEffects is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'UIViewWithVariousEffects'
```

## Author

RuslanMavlyutov, piercerum@gmail.com

## License

UIViewWithVariousEffects is available under the MIT license. See the LICENSE file for more info.
